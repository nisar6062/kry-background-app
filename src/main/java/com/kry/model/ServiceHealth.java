package com.kry.model;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceHealth {
	private String id;
	private String name;
	private String url;
	private String status;
	private Timestamp lastChecked;
	private boolean isDeleted;

	public ServiceHealth() {
		super();
	}

	public ServiceHealth(String name) {
		super();
		this.name = name;
	}

	public ServiceHealth(String name, String url, String status, Timestamp lastChecked) {
		super();
		this.name = name;
		this.url = url;
		this.status = status;
		this.lastChecked = lastChecked;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	public Timestamp getLastChecked() {
		return lastChecked;
	}

	public void setLastChecked(Timestamp lastChecked) {
		this.lastChecked = lastChecked;
	}

}
