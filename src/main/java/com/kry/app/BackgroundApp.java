package com.kry.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kry.model.ServiceHealth;
import com.kry.model.Status;
import com.kry.util.PropertyUtil;

public class BackgroundApp {
	private static final String NEW_LINE = "\n";

	private String fileBasePath;
	private String fileForFileName;
	private String fileForServiceData;
	private PropertyUtil propertyUtil;
	private static final Logger LOGGER = Logger.getLogger(BackgroundApp.class);

	public BackgroundApp() {
		propertyUtil = new PropertyUtil();
		fileBasePath = propertyUtil.getProperty("files-base-path");
		fileForFileName = fileBasePath + propertyUtil.getProperty("file-for-fileName");
		fileForServiceData = propertyUtil.getProperty("file-for-serviceData");
	}

	public static void main(String[] args) {
		BackgroundApp app = new BackgroundApp();
		app.run();
	}

	public void run() {
		int sleepTimeInSec = Integer.parseInt(propertyUtil.getProperty("sleep-time-in-secs"));
		while (true) {
			updateFile();
			try {
				Thread.sleep(sleepTimeInSec * 1000);
			} catch (InterruptedException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
	}

	public String checkAPIStatus(String url) {
		HttpClient client = new DefaultHttpClient();
		HttpGet request;
		try {
			request = new HttpGet(url);
			HttpResponse response = client.execute(request);
			int status = response.getStatusLine().getStatusCode();
			LOGGER.info(url + " REST API status: " + status);
			if (status == 200) {
				return Status.OK.toString();
			}
		} catch (HttpException | IOException | URISyntaxException e) {
			LOGGER.error(e.getMessage(), e);
		}
		return Status.FAIL.toString();
	}

	public String updatefileForFileName(String latestfileName) {
		FileReader fr = null;
		BufferedReader br = null;
		FileOutputStream fileOut = null;
		try {
			File f1 = new File(fileForFileName);
			fr = new FileReader(f1);
			br = new BufferedReader(fr);
			String oldFile = br.readLine();
			String newFile = br.readLine();
			LOGGER.info("oldFile: " + oldFile + "newFile: " + newFile);
			if (latestfileName != null) {
				newFile += NEW_LINE + latestfileName;
				fileOut = new FileOutputStream(fileForFileName);
				fileOut.write(newFile.getBytes());
			} else {
				return newFile;
			}
		} catch (FileNotFoundException e) {
			LOGGER.error(e.getMessage(), e);
			// For first time if the file is not created
			String fileName = fileForServiceData + new Date().getTime();
			updateServiceData(propertyUtil.getProperty("file-for-fileName"), Arrays.asList(fileName, fileName));
			updateServiceData(fileName, new ArrayList<>());
			return fileName;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);

		} finally {
			try {
				if (fr != null)
					fr.close();
				if (br != null)
					br.close();
				if (latestfileName != null && fileOut != null)
					fileOut.close();
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		return null;
	}

	public void updateServiceData(String latestfileName, List<String> lines) {
		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(fileBasePath + latestfileName);
			for (String line : lines) {
				fileOut.write((line + "\n").getBytes());
			}
			LOGGER.info("File wrote: " + latestfileName);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			try {
				if (fileOut != null)
					fileOut.close();
			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
	}

	public void updateFile() {
		ObjectMapper objMap = new ObjectMapper();
		String currentfileForFileName = updatefileForFileName(null);
		String line = null;
		FileReader fr = null;
		BufferedReader br = null;
		List<String> lines = new ArrayList<>();
		try {
			File file = new File(fileBasePath + currentfileForFileName);
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			while ((line = br.readLine()) != null) {
				ServiceHealth service = objMap.readValue(line, ServiceHealth.class);
				String status = checkAPIStatus(service.getUrl());
				service.setStatus(status);
				service.setLastChecked(new Timestamp(new Date().getTime()));
				lines.add(objMap.writeValueAsString(service));
			}
			if (lines.size() == 0) {
				return;
			}
			LOGGER.info("New Lines: " + lines);
			String latestfileForFileName = fileForServiceData + new Date().getTime();
			updateServiceData(latestfileForFileName, lines);
			updatefileForFileName(latestfileForFileName);
			deleteOldFile(currentfileForFileName);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			try {
				fr.close();
				br.close();
			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
	}

	private void deleteOldFile(String currentfileForFileName) {
		File file = new File(fileBasePath + currentfileForFileName);
		if (file.delete()) {
			LOGGER.debug(currentfileForFileName + " deleted successfully");
		} else {
			LOGGER.debug(currentfileForFileName + " failed to delete");
		}
	}
}
